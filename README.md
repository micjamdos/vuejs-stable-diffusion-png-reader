# vuejs-stable-diffusion-png-reader

VueJS app for reading Stable Diffusion prompt metadata from PNG images. Tested working on Firefox and Brave on Ubuntu 22.04 and Windows 10

## Setup

Clone the repo and run
```
npm install
```

## Run
```
npm run serve
```

## Known Issues
* On Linux, the flatpak version of Brave has issues with exifr parsing drag-and-drop images from certain directories. If the app crashes, move the image to a different directory and try drag-and-dropping again.